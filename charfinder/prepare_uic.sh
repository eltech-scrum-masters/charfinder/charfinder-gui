#!/bin/bash
BASE_DIR=$(dirname "${BASH_SOURCE[0]}")
for filename in "$BASE_DIR"/UI/*.ui; do
  pyuic5 "$filename" -o $(echo "$filename" | sed 's/\.ui/\.py/')
done