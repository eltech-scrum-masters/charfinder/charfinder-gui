from charfinder.UI import Ui_MainWindow
from charfinder.Config import API_ROOT
import requests as req


class Health:
    fieldMap = {
        'total_HP': 'HealthTotalHp',
        'DR': 'DR',
        'current_HP': 'CurrentHP',
        'nonlethal_damage': 'NonlethalDamage',
        'load': 'Load',
        'state': 'State'
    }

    def __init__(self, ui: Ui_MainWindow):
        for (json_field, line_edit_suffix) in Health.fieldMap.items():
            self.__setattr__(json_field, ui.__getattribute__(f'lineEdit{line_edit_suffix}'))

    def get_health(self, character: str):
        path = f'{API_ROOT}/characters/{character}/health'
        resp = req.get(path)
        if resp.status_code == 200:
            health = resp.json()['health'][0]
            for json_field in Health.fieldMap.keys():
                self.__getattribute__(json_field).setText(str(health[json_field]))
