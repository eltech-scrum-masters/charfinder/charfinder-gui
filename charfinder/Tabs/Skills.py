from charfinder.UI import Ui_MainWindow
from charfinder.Config import API_ROOT
from .Table import Table
import requests as req


class Skills(Table):
    def __init__(self, ui: Ui_MainWindow):
        super().__init__(ui.tableWidgetSkills)

    def set_row(self, row: int, skill: {}):
        self.set_item(row, 0, skill['name'])
        self.set_item(row, 1, skill['total'])
        self.set_item(row, 2, skill['ability_modifier'])
        self.set_item(row, 3, skill['ranks'])
        self.set_item(row, 4, skill['misc_modifier'])

    def get_skills(self, character: str):
        path = f'{API_ROOT}/characters/{character}/skills'
        resp = req.get(path)
        if resp.status_code == 200:
            skills = resp.json()['skills']
            self.clear_table()
            for idx, skill in enumerate(skills):
                self.add_row()
                self.set_row(idx, skill)
