from charfinder.UI import Ui_MainWindow
from charfinder.Config import API_ROOT
from .Table import Table
import requests as req


class Items(Table):
    def __init__(self, ui: Ui_MainWindow):
        super().__init__(ui.tableWidgetItems)

    def set_row(self, row: int, item: {}):
        self.set_item(row, 0, item['item_name'])
        self.set_item(row, 1, item['item_weight'])

    def get_gear(self, character: str):
        path = f'{API_ROOT}/characters/{character}/gear'
        resp = req.get(path)
        if resp.status_code == 200:
            gear = resp.json()['gear']
            self.clear_table()
            for idx, item in enumerate(gear):
                self.add_row()
                self.set_row(idx, item)
