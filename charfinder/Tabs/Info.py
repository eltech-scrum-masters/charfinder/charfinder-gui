from charfinder.UI import Ui_MainWindow
from charfinder.Config import API_ROOT
import requests as req


class Info:
    def __init__(self, ui: Ui_MainWindow):
        self.ui = ui

    def get_info(self, character: str):
        path = f'{API_ROOT}/characters/{character}/info'
        resp = req.get(path)
        if resp.status_code == 200:
            info = resp.json()['info'][0]
            self.ui.lineEditCharacterName.setText(info['full_name'])
            self.ui.lineEditAlignment.setText(info['alignment'])
            self.ui.lineEditCharacterLevel.setText(str(info['level']))
            self.ui.lineEditDeity.setText(info['deity'])
            self.ui.lineEditHomeland.setText(info['homeland'])
            self.ui.lineEditRace.setText(info['race'])
            self.ui.lineEditSize.setText(info['size'])
            self.ui.lineEditGender.setText(info['gender'])
            self.ui.lineEditAge.setText(str(info['age']))
            self.ui.lineEditHeight.setText(str(info['height']))
            self.ui.lineEditWeight.setText(str(info['weight']))
            self.ui.lineEditHair.setText(info['hair'])
            self.ui.lineEditEyes.setText(info['eyes'])
