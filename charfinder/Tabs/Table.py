from PyQt5.Qt import QTableWidget, QTableWidgetItem


class Table:
    def __init__(self, table: QTableWidget):
        self.table = table

    def add_row(self):
        self.table.insertRow(self.table.rowCount())

    def set_item(self, row: int, col: int, value: any):
        self.table.setItem(row, col, QTableWidgetItem(str(value)))

    def clear_table(self):
        while self.table.rowCount() > 0:
            self.table.removeRow(0)
