from charfinder.UI import Ui_MainWindow
from charfinder.Config import API_ROOT
from PyQt5.Qt import QLineEdit
import requests as req


class Abilities:
    class Ability:
        def __init__(self, score: QLineEdit, adjustment: QLineEdit, modifier: QLineEdit):
            self.score = score
            self.adjustment = adjustment
            self.modifier = modifier

        def clear_data(self):
            self.score.clear()
            self.adjustment.clear()
            self.modifier.clear()

        def set_data(self, ability: {}):
            self.score.setText(str(ability['score']))
            self.adjustment.setText(str(ability['temp_adjustment']))
            self.modifier.setText(str(ability['temp_modifier']))

    def __init__(self, ui: Ui_MainWindow):
        def create_ability(abi: str):
            return Abilities.Ability(
                ui.__dict__[f'lineEdit{abi}AbilityScore'],
                ui.__dict__[f'lineEdit{abi}TempAdjustment'],
                ui.__dict__[f'lineEdit{abi}TempModifier']
            )
        self.str = create_ability('Str')
        self.dex = create_ability('Dex')
        self.con = create_ability('Con')
        self.int = create_ability('Int')
        self.wis = create_ability('Wis')
        self.cha = create_ability('Cha')

    def get_abilities(self, character: str):
        path = f'{API_ROOT}/characters/{character}/abilities'
        resp = req.get(path)
        if resp.status_code == 200:
            abilities = resp.json()['abilities']

            def get_ability(name: str):
                return list(filter(lambda x: x['name'] == name, abilities))[0]

            self.str.set_data(get_ability('Strength'))
            self.dex.set_data(get_ability('Dexterity'))
            self.con.set_data(get_ability('Constitution'))
            self.int.set_data(get_ability('Intelligence'))
            self.wis.set_data(get_ability('Wisdom'))
            self.cha.set_data(get_ability('Charisma'))
