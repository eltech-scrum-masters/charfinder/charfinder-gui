from PyQt5.Qt import QMainWindow
from charfinder.Tabs import Info, Abilities, Health, Items, Skills
from charfinder.UI import Ui_MainWindow
from charfinder.SearchCharacterDialog import SearchCharacterDialog


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.search_dialog = SearchCharacterDialog(self)
        self.info = Info(self.ui)
        self.abilities = Abilities(self.ui)
        self.items = Items(self.ui)
        self.skills = Skills(self.ui)
        self.health = Health(self.ui)

        self.ui.pushButtonSelectCharacter.clicked.connect(self.spawn_search_dialog)

        # self.update_tabs('Babatunde')

    def update_tabs(self, character: str):
        self.info.get_info(character)
        self.abilities.get_abilities(character)
        self.health.get_health(character)
        self.items.get_gear(character)
        self.skills.get_skills(character)

    def spawn_search_dialog(self):
        self.search_dialog.show()
