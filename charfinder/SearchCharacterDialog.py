from PyQt5.Qt import QDialog, QHeaderView

from charfinder.Config import API_ROOT
from charfinder.UI import Ui_SearchChacterDialog
from charfinder.Tabs.Table import Table
from charfinder import MainWindow

import functools as func
import requests as req


class SearchCharacterDialog(QDialog):
    filter_list = ['name', 'alignment', 'deity', 'homeland', 'race', 'size', 'gender', 'hair', 'eyes']

    class SearchResults(Table):
        def __init__(self, ui: Ui_SearchChacterDialog):
            super().__init__(ui.charactersTable)
            self.table

        def set_row(self, row: int, item: {}):
            self.set_item(row, 0, item['character_name'])
            self.set_item(row, 1, item['full_name'])
            self.set_item(row, 2, item['alignment'])
            self.set_item(row, 3, item['deity'])
            self.set_item(row, 4, item['homeland'])
            self.set_item(row, 5, item['race'])
            self.set_item(row, 6, item['size'])
            self.set_item(row, 7, item['gender'])
            self.set_item(row, 8, item['hair'])
            self.set_item(row, 9, item['eyes'])

        def update_table(self, search_results: [{}]):
            self.clear_table()
            for idx, row in enumerate(search_results):
                self.add_row()
                self.set_row(idx, row)

    def __init__(self, parent: MainWindow):
        super().__init__()

        self.ui = Ui_SearchChacterDialog()
        self.ui.setupUi(self)
        self.ui.charactersTable.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
        self.parent = parent
        self.current_filters = {}
        self.searchResults = SearchCharacterDialog.SearchResults(self.ui)

        self.connect_enable_on_check()
        self.connect_update_on_change()
        self.ui.charactersTable.cellClicked.connect(self.on_cell_clicked)
        self.ui.cancelButton.clicked.connect(self.close)
        self.ui.okButton.clicked.connect(self.when_ok)
        self.update_search_results()
        self.update()
        self.result_name = ""

    def on_cell_clicked(self, row, col):
        self.ui.okButton.setEnabled(True)
        self.result_name = self.ui.charactersTable.item(row, 0).text()

    def connect_enable_on_check(self):
        for filter_name in SearchCharacterDialog.filter_list:
            self.ui.__getattribute__(f'{filter_name}_checkBox')\
                .clicked.connect(self.ui.__getattribute__(f'{filter_name}_lineEdit').setEnabled)
            self.ui.__getattribute__(f'{filter_name}_checkBox') \
                .clicked.connect(func.partial(self.filter_checkbox_changed, filter_name))

    def connect_update_on_change(self):
        for filter_name in SearchCharacterDialog.filter_list:
            self.ui.__getattribute__(f'{filter_name}_lineEdit').textChanged.connect(self.update_search_results)

    def filter_checkbox_changed(self, filter_name: str, state: bool):
        self.current_filters[filter_name] = state
        self.update_search_results()

    def update_search_results(self):
        path = f'{API_ROOT}/search'
        query = self.build_query()
        resp = req.put(path, json=query)
        if resp.status_code == 200:
            self.searchResults.update_table(resp.json())

    def build_query(self):
        query = {}
        for filter_name, used in self.current_filters.items():
            if used:
                query[filter_name] = self.ui.__getattribute__(f'{filter_name}_lineEdit').text()
        return query

    def when_ok(self):
        self.parent.update_tabs(self.result_name)
        self.close()